# Maintainer: Stefano Capitani <stefano[at]manjaro[dot]org>
# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Helmut Stult
# Contributor: Patrick Brisbin <pbrisbin@gmail.com>

pkgname=manjaro-downgrade
_pkgname=downgrade
pkgver=2.2
_pkgver=11.3.0
pkgrel=2
pkgdesc="Bash script for downgrading one or more packages to a version in your cache or the M.L.A./A.L.A."
arch=('any')
url="https://github.com/archlinux-downgrade/downgrade"
license=('GPL-2.0-or-later')
depends=('bash' 'fzf' 'pacman-contrib' 'pacman-mirrors')
checkdepends=('python-cram')
provides=("$_pkgname")
conflicts=("$_pkgname")
backup=("etc/xdg/$_pkgname/$_pkgname.conf")
source=("$_pkgname-$_pkgver.tar.gz::$url/archive/v$_pkgver.tar.gz"
        'manjaro-downgrade.patch')
sha256sums=('dfe4021d16eac329cd74b4a7a5ab4eac5e2a4a363f7d02ccaef576604c45ecdf'
            '8f4528e763be32d901f88a2510419916714f195cfe030eaa08fd17706021fbcc')

prepare() {
  cd "$_pkgname-$_pkgver"
  patch -Np1 -i "$srcdir/manjaro-downgrade.patch"
}

check() {
  cd "$_pkgname-$_pkgver"
  make test
}

package() {
  cd "$_pkgname-$_pkgver"
  make PREFIX=/usr DESTDIR="$pkgdir" install
  
  #set manjro versioning
  sed -i "s/""$_pkgver""/""$pkgver""/Ig" "$pkgdir/usr/bin/$_pkgname" 
  #set symlink to original executable
  ln -s "/usr/bin/$_pkgname" "$pkgdir/usr/bin/$pkgname"
}
